<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/abonnementsoffre-abonnements?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_abonnementsoffre' => 'Add this subscription offer',

	// C
	'champ_descriptif_label' => 'Description',
	'champ_duree_0' => 'No limit',
	'champ_duree_label' => 'Duration',
	'champ_periode_choix_heures' => 'Hours',
	'champ_periode_choix_jours' => 'Days',
	'champ_periode_choix_mois' => 'Month',
	'champ_periode_label' => 'Period type',
	'champ_periode_nb_heures' => '@nb@ hour(s)',
	'champ_periode_nb_jours' => '@nb@ day(s)',
	'champ_periode_nb_mois' => '@nb@ months',
	'champ_prix_0' => 'Free',
	'champ_prix_ht_label' => 'Price excl. tax',
	'champ_prix_label' => 'Price',
	'champ_prix_ttc_label' => 'Price incl. tax',
	'champ_quand_choix_apres' => 'After',
	'champ_quand_choix_avant' => 'Before',
	'champ_quand_choix_pendant' => 'The same day',
	'champ_quand_label' => 'When ?',
	'champ_taxe_label' => 'VAT (%)',
	'champ_titre_label' => 'Title',
	'configurer_notifications' => 'Configure the notifications',
	'configurer_notifications_quand' => 'When ?',

	// E
	'erreur_notification_doublon' => 'This notification is already registered',

	// I
	'icone_creer_abonnementsoffre' => 'Create a subscription offer',
	'icone_modifier_abonnementsoffre' => 'Modify this subscription offer',
	'info_1_abonnementsoffre' => 'A subscription offer', # MODIF
	'info_1_an' => '1 an', # MODIF
	'info_1_heure' => '1 heure', # MODIF
	'info_1_jour' => '1 jour', # MODIF
	'info_1_jours_apres' => '1 day after',
	'info_1_jours_avant' => '1 day before',
	'info_1_mois' => '1 month', # MODIF
	'info_1_mois_apres' => '1 month after',
	'info_1_mois_avant' => '1 month before',
	'info_abonnementsoffres_auteur' => 'Subscription offers of this author',
	'info_aucun_abonnementsoffre' => 'No subscription offer',
	'info_aucune_notification' => 'No notification',
	'info_nb_abonnementsoffres' => '@nb@ subscription offers',
	'info_nb_ans' => '@nb@ ans', # MODIF
	'info_nb_heures' => '@nb@ heures', # MODIF
	'info_nb_jours' => '@nb@ jours', # MODIF
	'info_nb_jours_apres' => '@nb@ days after',
	'info_nb_jours_avant' => '@nb@ days before',
	'info_nb_mois' => '@nb@ months', # MODIF
	'info_nb_mois_apres' => '@nb@ month after',
	'info_nb_mois_avant' => '@nb@ month before',

	// R
	'retirer_lien_abonnementsoffre' => 'Remove this subscription offer',
	'retirer_tous_liens_abonnementsoffres' => 'Remove all subscription offers',

	// T
	'texte_ajouter_abonnementsoffre' => 'Add a subscription offer',
	'texte_changer_statut_abonnementsoffre' => 'This subscription offer is:',
	'texte_creer_associer_abonnementsoffre' => 'Create and associate a subscription offer',
	'titre_abonnementsoffre' => 'Subscription offer',
	'titre_abonnementsoffres' => 'Subscription offers',
	'titre_abonnementsoffres_rubrique' => 'Subscription offers of the section',
	'titre_langue_abonnementsoffre' => 'Language in this subscription offer',
	'titre_logo_abonnementsoffre' => 'Logo of this subscription offer'
);
