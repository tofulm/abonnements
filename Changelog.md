# Plugin abonnements : notes de versions

## v3.6.x

Ajout des abonnements à un nombre de séances

## v3.5.x

Ajout de squelettes de résumés génériques pour les abonnements et les offres :

* inclure/resume/abonnement.html + inc-abonnement_footer.html
* inclure/resume/abonnements_offre.html + inc-abonnements_offre_footer.html
