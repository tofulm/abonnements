<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_modifier_credit_dist($arg = null) {

	if ($arg === null) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	list($id_abonnement,$nb_credit) = explode('/', $arg);

	if (intval($id_abonnement)) {
		if (!intval($nb_credit)) {
			$nb_credit = -1;
		} else {
			$nb_credit = intval($nb_credit);
		}
		include_spip('inc/autoriser');
		if (intval($nb_credit) < 0) {
			if (! autoriser("decrementer","abonnement", $id_abonnement)) {
				return false;
			}
		} else {
			if (! autoriser("incrementer","abonnement", $id_abonnement)) {
				return false;
			}
		}

		$r = sql_fetsel('type, credits', 'spip_abonnements', 'id_abonnement='.intval($id_abonnement));
		if ($r['type'] === 'seance') {
			if (
				!defined('CREDIT_NEGATIF')
				and intval($nb_credit) < 0
				and intval($r['credits']) <= 0
			) {
				$message_retour = _T('abonnement:solde_insuffisant');
			} else {
				$credits = intval($r['credits']) + $nb_credit;
				$set['credits'] = $credits;

				// Si apres l'action, le credit est = 0
				// on inactive l'abonnement
				if ($credits <= 0) {
					$set['statut'] = 'inactif';
				} else {
					$set['statut'] = 'actif';
				}

				$r = sql_updateq('spip_abonnements', $set, 'id_abonnement='.intval($id_abonnement));

				if (!$r) {
					pipeline("abonnement_modifier_credits" , [
						"args" => compact('id_abonnement', 'nb_credit'),
						"data" => $set
					]);
				}

				$message_retour = $credits;

				// Cache
				include_spip('inc/invalideur');
				suivre_invalideur("id='id_abonnement/$id_abonnement'");
			}
		} else {
			$message_retour = _T('abonnement:abonnement_pas_type_seance');
		}
	}
	return $message_retour;
}
