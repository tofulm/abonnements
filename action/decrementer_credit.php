<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * action de décrementer un abonnement de type "seance"
 *
 * @param $arg "id_abonnement-nombre"
 *
 * @return int|string
 *	- Int credits "credit" restants
 *	- false si boom
 *	- String : si pas assez de crédit
 *	- String si pas abonnement de type "seance"
 *
 */
function action_decrementer_credit_dist($arg = null){

	if ($arg === null) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	list($id_abonnement,$nb_credit) = explode('-', $arg);

	$res = false;
	if (intval($id_abonnement)) {
		if (!intval($nb_credit)) {
			$nb_credit = -1;
		} else {
			$nb_credit = intval($nb_credit);
		}

		$f = charger_fonction('modifier_credit', 'action');
		$res = $f($id_abonnement.'-'.$nb_credit);
	}

	return $res;
}
